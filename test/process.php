<?php
$weather = array();
$weather[] = "soleil";
$weather[] = "grêle";
$weather[] = "pluie";
$weather[] = "orage";
$temps = array();
for($i=0;$i<24;$i++){
    $temps[] = rand(15,25);
}
$data = array();
$data["time"] = date("Y-m-d H:i:s");
$data["weather"] = $weather[array_rand($weather)];
$data["temperatures"] = $temps;

$json = json_encode($data);
echo($json);
?>