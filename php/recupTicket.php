

<?php
require("bdd.php");

function ajouter(){
    $login = $_POST['login'];
    $sujet = $_POST['subject'];
    $description = $_POST['description'];
    $prio = $_POST['prio'];
    $secteur = $_POST['sector'];
    $statut = $_POST ['statut'];
    $idUser = $_SESSION['id'];
    $bdd = getBdd();
    
    //enregistrement et execution de la requête
    $req = $bdd->prepare("INSERT INTO ticket(login, sujet, description, prio, secteur, statut,idUser) VALUES(?,?, ?, ?,?,?,?)");
    $result = $req->execute(array($login, $sujet, $description, $prio, $secteur, $statut,$idUser));
    //Vérification pour savoir si l'inscription s'est bien déroulée
    if ($result==1) {
      echo "New ticket created with succes";
      echo "<li><a href='../index.php'>Retour vers la page d'accueil</a></li>";
    } else {
      echo "Impossible de créer l'enregistrement";
      echo "<li><a href='formTicket.html'>Retour vers la page de ticket</a></li>";
    }
  }

ajouter();


?>
