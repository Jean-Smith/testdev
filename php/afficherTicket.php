<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>All tickets</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href ="../index.css">
   </head>
   <body>
      <div class="container">
         <h2>All tickets</h2>
         <table class="table">
            <thead>
               <tr class = "ligne">
               <th>id</th>
                  <th>login</th>
                  <th>subject</th>
                  <th>description</th>
                  <th>priority</th>
                  <th>sector</th>
                  <th>statut</th>
                  <th>idUser</th>
               </tr>
            </thead>
            <tbody>
                <?php
                require("bdd.php");
                $bdd = getBdd();
                
                $resultats = $bdd->query("SELECT * FROM ticket ");
                $resultats->setFetchMode(PDO::FETCH_OBJ);
         
                if ($ligne = $resultats->fetch()) {
                    do {
                    ?>
                     <tr>
                     <td><?php echo                 ($ligne->id.'</p>');?></td>
                  <td><?php echo                 ($ligne->login.'</p>');?></td>
                  <td><?php echo                 ($ligne->sujet.'</p>');?></td>
                  <td><?php echo                 ($ligne->description.'</p>');?></td>
                  <td><?php echo                 ($ligne->prio.'</p>');?></td>
                  <td><?php echo                 ($ligne->secteur.'</p>');?></td>
                  <td><?php echo                 ($ligne->statut.'</p>');?></td>
                  <td><?php echo                 ($ligne->idUser.'</p>');?></td>
                  
                    </tr>
                        
                   <?php } while ($ligne = $resultats->fetch());
                } else {
                    echo "Pas d'info";
                }
                ?>
               
            </tbody>
         </table>
      </div>
      <form name="form" action="updateTicket.php" method="post">
      <div>Enter the id of the ticket that you want to set resolu </div>

        <input type="number" name="num" id="num" >
        <input id="OK_valide" type="submit" value="submit" class="bouton"/>   
        
      </form>
      <form name="form" action="afficherByIdTicket.php" method="post">
         <div>Enter the id of the ticket that you want to display </div>
        <input type="number" name="num" id="num" >
        <input id="OK_valide" type="submit" value="submit" class="bouton"/>   
      </form>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   </body>
</html>