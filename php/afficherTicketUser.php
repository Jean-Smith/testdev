<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>All tickets</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href ="../index.css">
   </head>
   <body>
      <div class="container">
         <h2>All tickets</h2>
         <table class="table">
            <thead>
               <tr class = "ligne">
                  <th>login</th>
                  <th>subject</th>
                  <th>description</th>
                  <th>priority</th>
                  <th>sector</th>
                  <th>statut</th>
               </tr>
            </thead>
            <tbody>
                <?php
                require("bdd.php");
                $bdd = getBdd();
                $id = $_SESSION['id'];
                $resultats = $bdd->query("SELECT * FROM ticket WHERE idUser = $id");
                $resultats->setFetchMode(PDO::FETCH_OBJ);
         
                if ($ligne = $resultats->fetch()) {
                    do {
                    ?>
                     <tr>
                  <td><?php echo                 ($ligne->login.'</p>');?></td>
                  <td><?php echo                 ($ligne->sujet.'</p>');?></td>
                  <td><?php echo                 ($ligne->description.'</p>');?></td>
                  <td><?php echo                 ($ligne->prio.'</p>');?></td>
                  <td><?php echo                 ($ligne->secteur.'</p>');?></td>
                  <td><?php echo                 ($ligne->statut.'</p>');?></td>
                    </tr>
                        
                   <?php } while ($ligne = $resultats->fetch());
                } else {
                    echo "Pas d'info";
                }
                ?>
               
            </tbody>
         </table>
      </div>
      
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
   </body>
</html>