<?php
// Initialisation de la session.

require("bdd.php");

// Détruit toutes les variables de session
$_SESSION = array();

// Si vous voulez détruire complètement la session, effacez également
// le cookie de session.
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finalement, on détruit la session.
session_destroy();
echo "Vous êtes bien deconnecté";
echo "<li><a href='../index.php'>Retour la page d'accueil</a></li>";
?>